# fish-functions
demo functions on how to write basic functions in fish

how to write basic functions in fish shell

# funced

First off to create a function while in the fish shell we will type "func" and then pass an arguemnt and that will be the function name you desire.

```sh
funced test
```
 You can pass these flags for 
 
- i | interactive mode
- e | open in external editor
- s | autosave after succesful edit
```sh
funced -i test
```
# funsave
 Save your function, test your function first to make sure it works.
 ```sh
 funsave test
 ```
 
 # example functions
 example functions 
 
 ### mkcd
 make dir and cd into it and also pwd
 
 ```sh
 	mkdir $argv && cd $argv && pwd
 ```
 
 ### gc
 cd into my project folder then git clone a project
 
 ```sh
	cd projects && git clone $argv
 ```
 
 ### ys
 
search the aur with yay -s

```sh
yay -s $argv
```

# You can now create basic funtions in fish!

here are my git files, check them out!
[fish-functions](https://github.com/georgenaranjo96/fish-functions.git)

